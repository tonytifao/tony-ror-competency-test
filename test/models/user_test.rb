require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'valid user' do
    user = User.new(email: 'user@user.com', role: 'admin', password: 'Abc1234')
    assert user.valid?
  end

  test 'user with invalid role' do
    user = User.new(email: 'user@user.com', role: 'invalid_role', password: 'Abc1234')
    assert_not user.valid?
  end

  test 'user with no password' do
    user = User.new(email: 'user@user.com', role: 'invalid_role')
    assert_not user.valid?
  end
end
