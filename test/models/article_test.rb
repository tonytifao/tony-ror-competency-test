require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  test 'valid article' do
    user = User.new(email: 'user@user.com', role: 'admin', password: 'Abc1234')
    article = Article.new(title: 'Ibiza', content: 'Party!', category: 'vacation', user: user)
    assert article.valid?
  end

  test 'invalid article if contains no user' do
    article = Article.new(title: 'Ibiza', content: 'Party!', category: 'vacation')
    article.valid?
    assert_equal(["User must exist"], article.errors.full_messages)
  end
end
