class Article < ApplicationRecord

  CATEGORIES =     [
    ['Computer Programming', 'computer_programming'],
    ['Vacation', 'vacation']
  ]

  belongs_to :user
  validates :category, inclusion: { in: CATEGORIES.collect{|cat| cat[1]}, message: "%{value} is not a valid category" }, allow_blank: false
end
