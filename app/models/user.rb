class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  VALID_ROLES = ["admin", "editor", "user"]

  validates :role, inclusion: { in: VALID_ROLES, message: "%{value} is not a valid role" }, allow_blank: false
  after_initialize :set_default_role, :if => :new_record?

  def admin?
    role == "admin"
  end

  def editor?
    role == "editor"
  end

  def set_default_role
    self.role ||= "user"
  end
end

