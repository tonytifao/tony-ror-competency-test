class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:index]

  def index
    authorize User, :index?
    @users = User.all
  end
end
