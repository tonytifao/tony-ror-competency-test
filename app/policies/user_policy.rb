class UserPolicy < ApplicationPolicy
    attr_reader :user, :article
  
    def initialize(user, resource)
      @user = user
      @resource = resource
    end
  
    def index?
      user.admin?
    end

    def create_article?
        user && user.editor?
    end
  end