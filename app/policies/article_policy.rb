class ArticlePolicy < ApplicationPolicy
    attr_reader :user, :article
  
    def initialize(user, article)
      @user = user
      @article = article
    end
  
    def update?
      user && user.editor?
    end

    def edit?
      user && user.editor? && article.user == user
    end

    def destroy?
      user && user.editor? && article.user == user
    end
  end