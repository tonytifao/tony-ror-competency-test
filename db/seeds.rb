# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Article.destroy_all
User.destroy_all

User.create(email: 'admin@gmail.com', password: 'Abc1234', role: 'admin')
User.create(email: 'user@gmail.com', password: 'Abc1234')

e1 = User.create(email: 'editor1@gmail.com', password: 'Abc1234', role: 'editor')
e2 = User.create(email: 'editor2@gmail.com', password: 'Abc1234', role: 'editor')

Article.create(title: 'The best of Ruby', content: 'Ruby is an awesome language. Rails is an awesome framework', category: 'computer_programming', user: e1)
Article.create(title: 'Ibiza', content: 'Party!', category: 'vacation', user: e1)
Article.create(title: 'Tahiti', content: 'Paradise', category: 'vacation', user: e2)
Article.create(title: 'Barcelona', content: 'Spain!!!!', category: 'vacation', user: e1)
Article.create(title: 'Rio', content: 'Rio de Janeiro', category: 'vacation', user: e2)



