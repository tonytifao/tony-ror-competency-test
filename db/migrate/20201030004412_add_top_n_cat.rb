class AddTopNCat < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE VIEW top_n_articles AS
        SELECT 
        articles.*,
        ROW_NUMBER() OVER (PARTITION BY articles.category ORDER BY articles.id DESC) AS RN
        FROM articles
    SQL
  end

  def down
    execute("DROP VIEW top_n_articles")
  end
end
